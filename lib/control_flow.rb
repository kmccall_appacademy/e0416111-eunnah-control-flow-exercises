# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete("abcdefghijklmnopqrstuvwxyz")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_index = str.length/2
  return str[(mid_index-1)..mid_index] if str.length.even?

  str[mid_index]
end

# Return the number of vowels in a string.
#VOWELS = %w(a e i o u)
def num_vowels(str)
  vowels = "aeiouAEIOU"
  str.count(vowels)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  (1..num).each do |el|
    product = product * el
  end

  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
new_string = ""
  arr.each_index do |idx|
    if idx == 0
      new_string = arr[idx]
    else
      new_string = new_string + separator + arr[idx]
    end
  end
  new_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = ""
  str.chars.each_index do |idx|
    if idx.even?
      new_str += str[idx].downcase
    else
      new_str += str[idx].upcase
    end
  end

  new_str
end

# Reverse all words of five or more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split
  result_arr = []

  arr.each do |word|
    if word.length >= 5
      result_arr << word.reverse
    else
      result_arr << word
    end
  end
  result_arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []
  (1..n).each do |el|
    if el % 3 == 0 && el % 5 == 0
      arr << "fizzbuzz"
    elsif el % 3 == 0
      arr << "fizz"
    elsif el % 5 == 0
      arr << "buzz"
    else
      arr << el
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []

  arr.each { |el| reversed.unshift(el) }

  reversed 
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1
  (2...num).each do |divisor|
    return false if num % divisor == 0
  end

  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = []

  (1..num).each do |factor|
    arr << factor if num % factor == 0
  end
  arr

end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  all_factors = factors(num)
  arr = []
  all_factors.each { |el| arr << el if prime?(el) }
  arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even_arr = []
  odd_arr = []
  arr.each do |num|
    if num.even?
      even_arr << num
    else
      odd_arr << num
    end
  end

  if even_arr.length == 1
    even_arr[0]
  else
    odd_arr[0]
  end
end
